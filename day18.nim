# https://adventofcode.com/2022/day/18

import os, strutils, strscans

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  Cube = tuple[x, y, z: int]

var
  cubes: seq[Cube] = @[]
  minX, minY, minZ, maxX, maxY, maxZ: int

# Here, we use the flood fill approach mentioned in the subreddit discussion.
# https://www.reddit.com/r/adventofcode/comments/zoqhvy/2022_day_18_solutions/
# Solution is based on 
# https://github.com/balintbalazs/advent-of-code-2022/blob/main/src/bin/day18.rs

# Build list of cubes  
block readInput:
  minX = int.high
  minY = int.high
  minZ = int.high
  maxX = int.low
  maxY = int.low
  maxZ = int.low

  for line in readFile(os.paramStr(1)).strip().splitLines():
    var
      x, y, z: int
    
    if line.scanf("$i,$i,$i", x, y, z):
      if x > maxX:
        maxX = x
      
      if x < minX:
        minX = x
      
      if y > maxY:
        maxY = y
      
      if y < minY:
        minY = y
        
      if z > maxZ:
        maxZ = z
      
      if z < minZ:
        minZ = z  
      
      cubes.add((x: x, y: y, z: z))

# Surface area of cubes excluding air pockets
block part1:
  var
    sharedSurfaces: int = 0
  
  # For each cube, surface area increases by 6 units. For each shared surface, 
  # it decreases by 1 unit.  
  for cube in cubes:
    sharedSurfaces.inc(6)
          
    for dx in -1 .. 1:
      for dy in -1 .. 1:
        for dz in -1 .. 1:   
          # Allow only the six corner positions
          if abs(dx) + abs(dy) + abs(dz) != 1:
            continue
          
          # Check co-ordinates before proceeding             
          if (cube.x + dx) < minX or (cube.x + dx) > maxX or 
            (cube.y + dy) < minY or (cube.y + dy) > maxY or 
            (cube.z + dz) < minZ or (cube.z + dz) > maxZ:
            continue
          
          # Check if it is a cube
          if cubes.contains((x: cube.x + dx, y: cube.y + dy, z: cube.z + dz)):
            sharedSurfaces.dec()

  echo "Part 1: ", sharedSurfaces

# Here, we need to account for air pockets. One way to do this is to calculate 
# the area using the air immediately surrounding the cubes. In order for this to
# work, the max and min values of the co-ordinates need to be adjusted to allow
# this outer layer to be considered. For this reason also, using a three 
# dimensional array causes problems when the minimum value before adjustment is 0.
block part2:
  # Adjust minimum and maximum limits
  minX = minX - 1
  minY = minY - 1
  minZ = minZ - 1
  maxX = maxX + 1
  maxY = maxY + 1
  maxZ = maxZ + 1
  
  # Recursively visit each outer cube
  var
    next: seq[Cube] = @[(x: minX, y: minY, z: minZ)]
    visited: seq[Cube] = @[]
    outsideSurf: int = 0
  
  # Is there data to be processed?
  while next.len() > 0:
    var
      current: Cube
      
    current = next[0]
    next.delete(0)
    
    # Prevent repeat processing
    if visited.contains(current):
      continue
    
    visited.add(current)
    
    # Examine the adjacent positions
    for dx in -1 .. 1:
      for dy in -1 .. 1:
        for dz in -1 .. 1:                
          # Allow only the six corner positions
          if abs(dx) + abs(dy) + abs(dz) != 1:
            continue
          
          # Check if the generated co-ordinates are valid
          if (current.x + dx) < minX or (current.x + dx) > maxX or 
            (current.y + dy) < minY or (current.y + dy) > maxY or 
            (current.z + dz) < minZ or (current.z + dz) > maxZ:
            continue
          
          # If the adjacent position contains a solid cube, then the current 
          # cube belongs to the immediate outer layer
          if cubes.contains((x: current.x + dx, y: current.y + dy, 
            z: current.z + dz)):
            outsideSurf.inc()
          else:
            # Add to queue for processing
            next.add((x: current.x + dx, y: current.y + dy, z: current.z + dz))

  echo "Part 2: ", outsideSurf
        
