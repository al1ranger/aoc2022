# https://adventofcode.com/2022/day/14

import os, strutils, strscans

# File must be provided
if os.paramCount() < 1:
  quit(1)

# Here it is assumed that the grid size will not exceed 1000 by 1000. In case 
# the input is for a larger grid, adjust the array size as required.
  
var
  grid: array[0..999, array[0..999, char]]
  minX, maxX, minY, maxY: int
  
block readInput:
  # Initialise
  for row in grid.low() .. grid.high():
    for col in grid[row].low() .. grid[row].high():
      grid[row][col] = '.'
  
  minX = grid.high()
  minY = grid.high()
  maxX = grid.low()
  maxY = grid.low()
  
  # Add the rock paths
  for line in readFile(os.paramStr(1)).strip().splitLines():
    var
      startX, startY, endX, endY: int
    
    startX = -1
    startY = -1
    endX = -1
    endY = -1
    
    for point in line.split("->"):
      # Read the first point
      if startX < 0:
        if point.strip().scanf("$i,$i", startX, startY):
          if startX < minX:
            minX = startX
          elif startX > maxX:
            maxX = startX
           
          if startY < minY:
            minY = startY
          elif startY > maxY:
            maxY = startY
          
        continue
      
      # Read the next point
      if point.strip().scanf("$i,$i", endX, endY):
        if endX < minX:
          minX = endX
        elif endX > maxX:
          maxX = endX
         
        if endY < minY:
          minY = endY
        elif endY > maxY:
          maxY = endY
      
      # Add the rock path    
      if endX == startX:
        var
          currentY, deltaY: int
        
        currentY = startY
        deltaY = (endY - startY) div abs(endY - startY)
        
        while currentY != endY:
          grid[currentY][startX] = '#'
          currentY.inc(deltaY)
        grid[endY][startX] = '#'
      
      if endY == startY:
        var
          currentX, deltaX: int
        
        currentX = startX
        deltaX = (endX - startX) div abs(endX - startX)
        
        while currentX != endX:
          grid[startY][currentX] = '#'
          currentX.inc(deltaX)
        grid[startY][endX] = '#'
      
      # Set starting point for next pass
      startX = endX
      startY = endY

# Function to simulate sand flow. Tries to simulate one step of the sand flow
# according to the rules of the problem. Returns true if there was a change in 
# the position, else returns false.
proc simulateSandFlow(sandX, sandY: var int): bool =
  result = false
  
  # Try to flow vertically down  
  if grid[sandY + 1][sandX] == '.':
    grid[sandY][sandX] = '.'
    grid[sandY + 1][sandX] = '0'
    sandY = sandY + 1
    return true
  
  # Try to flow diagonally to the left
  if grid[sandY + 1][sandX - 1] == '.':
    grid[sandY][sandX] = '.'
    grid[sandY + 1][sandX - 1] = '0'
    sandY = sandY + 1
    sandX = sandX - 1
    return true
  
  # Try to flow diagonally to the right
  if grid[sandY + 1][sandX + 1] == '.':
    grid[sandY][sandX] = '.'
    grid[sandY + 1][sandX + 1] = '0'
    sandY = sandY + 1
    sandX = sandX + 1
    return true
  
# Simulate sand flow without any floor
block part1:
  var
    sandX, sandY, particles: int
  
  particles = 0
  sandX = 500
  sandY = 0
  
  # Run the simulation till a grain of sand moves below the lowest obstruction
  while sandY < maxY:    
    # Try to simulate one step
    if simulateSandFlow(sandX, sandY):
      continue
    
    # Start with next grain of sand
    sandY = 0
    sandX = 500
    particles.inc()
  
  echo "Part 1: ", particles

# Simulate sand flow with a floor
block part2:
  var
    sandX, sandY, particles: int
    changed: bool
  
  particles = 0
  sandX = 500
  sandY = 0
  changed = true
  
  # Remove sand and add floor
  for y in grid.low() .. grid.high():
    for x in grid[y].low() .. grid[y].high():
      if grid[y][x] == '0':
        grid[y][x] = '.'
    
      if y == maxY + 2:
        grid[y][x] = '#'
  
  # Simulate flow till it stops
  while changed: 
    # Try to simulate one step
    changed = simulateSandFlow(sandX, sandY)
    if changed:
      continue
      
    # There was some movement for the grain of sand
    if sandY != 0:
      changed = true
    
    # Start with next grain of sand  
    sandY = 0
    sandX = 500
    grid[sandY][sandX] = '0'
    particles.inc()
  
  echo "Part 2: ", particles
