# https://adventofcode.com/2022/day/5

import os, strutils, strscans, sequtils

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  MoveCommand = tuple[count: int, src: int, dest: int]

var
  startingArrangement: seq[seq[char]] = @[]
  stackIds: seq[int] = @[]
  moveCommands: seq[MoveCommand] = @[]
 
# Read the input file 
block readInput:
  var
    instructionsSection: bool = false
  
  for line in readFile(os.paramStr(1)).splitLines():
    # Empty line separating the crate data from the movement instructions
    if line.len() == 0:
      instructionsSection = true
      continue
      
    if instructionsSection:
      var
        count, src, dest: int
      
      # Get and store the movement parameters
      if not line.scanf("move $i from $i to $i", count, src, dest):
        continue
      
      moveCommands.add((count : count, src : src, dest : dest))
      continue
    
    if line.find('[') == -1: # The stack numbers
      for stack in line.strip().split(' '):
        if stack.len() == 0: # Ignore blanks
          continue
        stackIds.add(stack.strip().parseInt())
      
      # If stacks are provided in numerical order, then the list of stackIDs 
      # is not needed
      block checkStackIDs:
        for stackIdx in 0 ..< stackIds.len():
          if stackIds[stackIdx] != stackIdx + 1:
            break checkStackIDs
        
        stackIds = @[]
        continue
    
    for idx in countup(0, line.len() - 1, 4): # Line containing crate details
      var
        seqIdx: int
      
      # Each entry is four characters long [<letter>]<space>
      # Blanks are present when there is no data for any stack
      seqIdx = idx div 4 
      
      # Create sequence for stack for first time use
      if startingArrangement.len() <= seqIdx:
        startingArrangement.add(@[])
      
      # Here, index 0 corresponds to the bottom of the stack
      if line[idx + 1] != ' ': # Ignore blanks
        startingArrangement[seqIdx].insert(line[idx + 1], 0)

# The processing of both the parts can be combined into a single function.
# The preserveOrder parameter will control how the movement happens.
proc processMoves(preserveOrder: bool = false): seq[seq[char]] =
  # Initialize to the state before the move
  result = startingArrangement
  
  # Process the movement instructions
  for command in moveCommands:
    var
      src, dest, srcIdx: int
    
    # If stack numbers are provided in order, the sequence index is simply the 
    # stack number minus 1. Otherwise, the index corresponds to the index  
    # where the stack number is found in the stackIds sequence.
    if stackIds.len() != 0:
      src = stackIds.find(command.src)
      dest = stackIds.find(command.dest)
    else:
      src = command.src - 1
      dest = command.dest - 1
    
    for i in countdown(command.count - 1, 0):    
      # Here, index 0 corresponds to the bottom of the stack, and the high 
      # function will give use the index for the top of the stack.
      # The add function will append to the sequence, i,e always add a crate 
      # to the top of the stack.
      if preserveOrder:
        # Because order needs to be preserved, we will start from the bottom 
        # most crate.
        srcIdx = result[src].high() - i
      else:
        srcIdx = result[src].high()
    
      # Carry out the transfer
      result[dest].add(result[src][srcIdx])
      result[src].delete(srcIdx)
  
# Transferring one crate at a time    
block part1:
  # Use foldl template to get the top crate of each stack
  echo "Part 1: ", processMoves().foldl(a & b[b.high()], "")

# Transferring crates but preserving the order  
block part2:
  # Use foldl template to get the top crate of each stack  
  echo "Part 2: ", processMoves(true).foldl(a & b[b.high()], "")
