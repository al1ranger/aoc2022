# https://adventofcode.com/2022/day/16

import os, strutils, strscans, sequtils

# File must be provided
if os.paramCount() < 1:
  quit(1)

var
  valveIds: seq[string] = @[]
  flowRates: seq[int] = @[]
  distances: seq[seq[int]] = @[]

block readInput:
  type
    Room = object
      valve: string
      tunnels: seq[string]
  
  var
    rooms: seq[Room] = @[]
  
  # Build links between the valves
  for line in readFile(os.paramStr(1)).strip().splitLines():
    var
      valve, tunnels: string
      flowRate: int
    
    if not(line.strip().scanf(
      "Valve $w has flow rate=$i; tunnels lead to valves $*", valve, flowRate, 
      tunnels) or line.strip().scanf(
      "Valve $w has flow rate=$i; tunnel leads to valve $*", valve, flowRate, 
      tunnels)):
      continue
      
    if valveIds.find(valve) == -1:
      rooms.add(Room(valve: valve, tunnels: @[]))
      valveIds.add(valve)
      flowRates.add(flowRate)
    
    var
      idx: int
    
    idx = valveIds.find(valve)
    for tunnel in tunnels.split(","):
      rooms[idx].tunnels.add(tunnel.strip())
  
  # Store distances between the valves. For combinations where a direct path is 
  # not specified, initialise distance as the number of valves.
  for src in rooms.low() .. rooms.high():
    distances.add(repeat(rooms.len(), rooms.len()))
    
    for dest in rooms.low() .. rooms.high():
      if src == dest:
        distances[src][dest] = 0
      
      if rooms[src].tunnels.contains(rooms[dest].valve):
        distances[src][dest] = 1
  
# From the problem description, it is clear that a valve may or may or may not 
# be opened the each time it is encountered. This means that the number of 
# options to be explored is not suitable for a brute force approach, and some
# optimization techniques need to be employed. The subreddit provides some help.
# https://www.reddit.com/r/adventofcode/comments/zn6k1l/2022_day_16_solutions/
#
# Calculate shortest distance between two valves using Floyd-Warshall algorithm
for waypoint in valveIds.low() .. valveIds.high():
  for src in valveIds.low() .. valveIds.high():
    for dest in valveIds.low() .. valveIds.high():
      var
        dist: int
        
      dist = distances[src][waypoint] + distances[waypoint][dest]
      if dist < distances[src][dest]:
        distances[src][dest] = dist

# Calculate the maximum pressure release. start is the last of starting 
# positions for the processors, valvePositions contains whether the valves are 
# open or closed, duration is a list of time remaining for each processor, 
# currentProcessor is the number of the current processor.
#
# Depending on the puzzle input, this will take some time to calculate the 
# result for part 2, but performance could be improved using some of the  
# approaches mentioned in the subredit.
proc maxPressureRelease(start: seq[int], valvePositions: string, 
  duration: seq[int], currentProcessor: int = 1): int =
  result = 0
  
  # Check whether any valve remains to be opened
  if valvePositions.find('0') == -1:
    return
  
  let
    processorIdx: int = currentProcessor - 1
    
  # Check whether time for the current processor has expired
  if duration[processorIdx] <= 0:
    return
  
  var
    valveIdx, timeRequired: int
    newValvePositions: string = valvePositions
    newDuration: seq[int] = duration
    pressures: seq[int] = @[0]
    current: seq[int] = start
  
  # Identify the location of the current processor
  valveIdx = current[processorIdx]
  
  # Try to decide the next valve to be opened
  for idx in valveIds.low() .. valveIds.high():
    if flowRates[idx] == 0 or valvePositions[idx] == '1':
      continue
    
    # Check if this valve can be opened ignoring all other valves in between
    timeRequired = distances[valveIdx][idx] + 1
    if timeRequired >= duration[processorIdx]:
      continue
    
    newValvePositions[idx] = '1' # Open valve
    current[processorIdx] = idx # Update position
    newDuration[processorIdx].dec(timeRequired) # Update time remaining
    
    # Have all processors finished for this duration
    if currentProcessor > 1:
      # Recursive call for the next processor
      pressures.add(flowRates[idx] * (duration[processorIdx] - timeRequired) + 
      current.maxPressureRelease(newValvePositions, newDuration, processorIdx))
    else:
      # Recursive call for the next round
      pressures.add(flowRates[idx] * (duration[processorIdx] - timeRequired) + 
      current.maxPressureRelease(newValvePositions, newDuration, start.len()))
    
    # Restore old values for next loop pass
    newValvePositions[idx] = '0'
    current[processorIdx] = start[processorIdx]
    newDuration[processorIdx] = duration[processorIdx]
    
  result = pressures.max()

# Maximum pressure release possible for a single processor in 30 minutes
block part1:         
  echo "Part 1: ", maxPressureRelease(@[valveIds.find("AA")], 
    repeat('0', valveIds.len()), @[30])

# Maximum pressure release possible for two processors in 26 minutes  
block part2:
  echo "Part 2: ", maxPressureRelease(@[valveIds.find("AA"), 
    valveIds.find("AA")], repeat('0', valveIds.len()), @[26, 26], 2)
