# https://adventofcode.com/2022/day/20

import os, strutils, sequtils

# File must be provided
if os.paramCount() < 1:
  quit(1)

var
  encrypted: seq[int] = @[]

# Get the list of numbers  
block readInput:
  for line in readFile(os.paramStr(1)).strip().splitLines():
    encrypted.add(line.parseInt())

# As hinted in the subreddit, the input contains duplicate values. This can also
# be observed by looking at the input file as well as by other means, such as 
# importing the data in a spreadsheet application and deleting the duplicates.
# So, we need to use both the number and its index while moving the data.
# https://www.reddit.com/r/adventofcode/comments/zqezkn/2022_day_20_solutions/

# Performs the mixing process using the input encrypted list of numbers. The
# rounds parameter defines the number of times the mixing occurs.
proc mixAndDecrypt(encrypted: seq[int], rounds: int = 1): int =
  result = 0
  
  var
    mixedNumbers: seq[string] = @[]
    srcIdx, destIdx: int
  
  # To handle repeated values, create a key from index and number
  for idx in encrypted.low() .. encrypted.high():
    mixedNumbers.add($idx & '_' & $encrypted[idx])
  
  # The actual mixing starts
  for round in 1 .. rounds: 
    # Process each number 
    for idx in encrypted.low() .. encrypted.high():
      var
        tempNumbers: seq[string] = @[]
        key: string
      
      # No movement for zro
      if encrypted[idx] == 0:
        continue
      
      # Build the key and identify source and destination
      key = $idx & '_' & $encrypted[idx]
      srcIdx = mixedNumbers.find(key)
      # Index goes from mixedNumbers.low() to mixedNumbers.high()
      destIdx = (srcIdx + encrypted[idx]) mod mixedNumbers.high()
      
      # Adjust the destination if required
      if destIdx <= mixedNumbers.low():
        destIdx.inc(mixedNumbers.high())
      
      # Nothing to do if source and destination are the same
      if srcIdx == destIdx:
        continue
      
      # Perform the movement      
      if srcIdx < destIdx:      
        tempNumbers.add(mixedNumbers[mixedNumbers.low() ..< srcIdx])
        tempNumbers.add(mixedNumbers[srcIdx + 1 .. destIdx])
        tempNumbers.add(mixedNumbers[srcIdx])
        tempNumbers.add(mixedNumbers[destIdx + 1 .. mixedNumbers.high()])
      else:
        tempNumbers.add(mixedNumbers[mixedNumbers.low() ..< destIdx])
        tempNumbers.add(mixedNumbers[srcIdx])
        tempNumbers.add(mixedNumbers[destIdx ..< srcIdx])
        tempNumbers.add(mixedNumbers[srcIdx + 1 .. mixedNumbers.high()])
      
      # Prepare for the next iteration
      mixedNumbers = tempNumbers
  
  # Locate the zero, find the required numbers and calculate the sum
  let
    zeroIndexKey = $encrypted.find(0) & "_0"
    
  result = mixedNumbers[(mixedNumbers.find(zeroIndexKey) + 1000) mod 
      mixedNumbers.len()].split("_")[1].parseInt() +  
      mixedNumbers[(mixedNumbers.find(zeroIndexKey) + 2000) mod 
      mixedNumbers.len()].split("_")[1].parseInt() +  
      mixedNumbers[(mixedNumbers.find(zeroIndexKey) + 3000) mod 
      mixedNumbers.len()].split("_")[1].parseInt()

# One round of mixing
block part1:
  echo "Part 1:", encrypted.mixAndDecrypt()

# Ten rounds of mixing for data multiplied by a decryption key
block part2:
  echo "Part 2: ", 
    encrypted.map(proc (x: int): int = x * 811589153).mixAndDecrypt(10)  
  
