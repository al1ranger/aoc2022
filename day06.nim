# https://adventofcode.com/2022/day/6

import os, strutils, sets

# File must be provided
if os.paramCount() < 1:
  quit(1)

# There is only one line in the file  
let data = readFile(os.paramStr(1)).splitLines()[0].strip()

# The processing of both the parts can be combined into a single function.
# The markerLength parameter controls the length of the unique string marker to
# search for.
proc findDataStart(markerLength: int) : int =
  result = 0
  
  # Create a set using the marker length. If all characters are unique, the 
  # length of the set will equal the marker length.
  for i in 0 .. data.len() - markerLength:
    var
      marker: HashSet[char]
      
    # Create a set using the substring under consideration 
    marker = data[i .. i + markerLength - 1].toHashSet()
    
    # There will be no change in length if all characters are unique
    if marker.len() == markerLength:
      return i + markerLength

# Locate a marker with length 4
block part1:
  echo "Part 1: ", findDataStart(4)

# Locate a marker of length 14
block part2:
  echo "Part 2: ", findDataStart(14)
