# https://adventofcode.com/2022/day/8

import os, strutils, sequtils, math, sugar

# File must be provided
if os.paramCount() < 1:
  quit(1)

var
  treeHeights: seq[seq[int]] = @[]

# Build a square array of tree heights
block readFile:    
  for line in readFile(os.paramStr(1)).strip().splitLines():
    treeHeights.add(@[])
    
    for treeHeight in line:
      # '0' is 48 and '9' is 57
      # As a new entry is added for every line, treeHeights.high() always gives 
      # the index of the most recently added line
      treeHeights[treeHeights.high()].add(treeHeight.ord() - 48)
      

# Number of visible trees 
block part1:
  # Returns an array indicating the visibility status of trees. If a tree is 
  # visible, the corresponding cell in the array will be 1, else 0.
  proc getVisibilityMap(trees: seq[seq[int]]): seq[seq[int]] =
    result = @[]
    
    for row in trees.low() .. trees.high():
      # Initialize all trees in the row as invisible
      result.add(repeat(0, trees[row].len()))
      
      for col in trees[row].low() .. trees[row].high():      
        # The first and last rows are always visible
        if row == trees.low() or row == trees.high():
          result[row][col] = 1
          continue
        
        # The first and last column are always visible
        if col == trees[row].low() or col == trees[row].high():
          result[row][col] = 1
          continue
        
        # Check all trees to the left and right of this tree 
        if trees[row][trees[row].low() ..< col].max() < trees[row][col] or 
          trees[row][col + 1 .. trees[row].high()].max() < trees[row][col]:
          result[row][col] = 1
          continue
        
        # Check all trees above this tree
        result[row][col] = 1
        for rowIdx in trees.low() ..< row:        
          if trees[rowIdx][col] >= trees[row][col]:
            result[row][col] = 0
            break
        
        if result[row][col] == 1:
          continue
        
        # Check all trees below this tree
        result[row][col] = 1    
        for rowIdx in row + 1 .. trees.high():
          if trees[rowIdx][col] >= trees[row][col]:
            result[row][col] = 0
            break
    
  # Calculate the row wise totals and then, the final total
  let rowVisibilityCounts = collect(newSeq):
    for row in treeHeights.getVisibilityMap():
      row.sum()
  
  echo "Part 1: ", rowVisibilityCounts.sum()

# Maximum visibility score
block part2:
  # Returns an array containing the visibility scores of trees
  proc getVisibilityScoreMap(trees: seq[seq[int]]): seq[seq[int]] =
    result = @[]
    
    for row in trees.low() .. trees.high():
      # Initialize score for all trees in the row as 0
      result.add(repeat(0, trees[row].len()))
      
      # Calculate the scores for all trees in the row.
      # A tree of equal or more height will block the view.
      # As the score is a product, the total score is 0 if any one score is 0
      for col in trees[row].low() .. trees[row].high():      
        var
          left, right, top, bottom: int = 0
        
        # Calculate score for visibility above        
        for rowIdx in countdown(row - 1, trees.low()):          
          top.inc()
          
          if trees[rowIdx][col] >= trees[row][col]:
            break
        
        if top == 0:
          result[row][col] = 0
          continue
        
        # Calculate score for visibility below
        for rowIdx in row + 1 .. trees.high():
          bottom.inc()
          
          if trees[rowIdx][col] >= trees[row][col]:
            break
        
        if bottom == 0:
          result[row][col] = 0
          continue
        
        # Calculate score for visibility to the left
        for colIdx in countdown(col - 1, trees[row].low()):               
          left.inc()
          
          if trees[row][colIdx] >= trees[row][col]:
            break
        
        if left == 0:
          result[row][col] = 0
          continue
        
        # Calculate score for visibility to the right
        for colIdx in col + 1 .. trees[row].high():          
          right.inc()
          
          if trees[row][colIdx] >= trees[row][col]:
            break
        
        if right == 0:
          result[row][col] = 0
          continue
        
        # Calculate the final visibility score
        result[row][col] = left * right * top * bottom
  
  # Calculate the row wise maximum and then, the overall maximum score
  let maxRowVisibilityScores = collect(newSeq):
    for row in treeHeights.getVisibilityScoreMap():
      row.max()
  
  echo "Part 2: ", maxRowVisibilityScores.max()
