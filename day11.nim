# https://adventofcode.com/2022/day/11

import os, strutils, strscans, parseutils, algorithm, math, sequtils

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  Monkey = object
    id, inspectionCount, divisor, falseRecv, trueRecv: int
    items: seq[int]
    operator:char
    rhs: string

var
  monkeys: seq[Monkey] = @[]

# Process the input file to create a sequence of Monkey objects  
block readInput:
  for line in readFile(os.paramStr(1)).strip().splitLines():
    if line.len() == 0 or monkeys.len() == 0:
      monkeys.add(Monkey())
      monkeys[monkeys.high()].inspectionCount = 0
      
    if line.len() == 0:
      continue
    
    var
      id, divisor, receiver, rhsInt: int
      itemsList, rhsString: string
      operator: char
    
    # The monkey number
    if line.scanf("Monkey $i:", id):
      monkeys[monkeys.high()].id = id
      continue
    
    # The initial list of items
    if line.strip().scanf("Starting items: $*", itemsList):
      monkeys[monkeys.high()].items = @[]
      
      for item in itemsList.strip().split(','):
        monkeys[monkeys.high()].items.add(item.strip().parseInt())
        
      continue
    
    # The calculation operation defined using the current value
    if line.strip().scanf("Operation: new = old $c $w", operator, rhsString):
      monkeys[monkeys.high()].operator = operator
      monkeys[monkeys.high()].rhs = rhsString
      continue
    
    # The calculation operation defined using an integer value  
    if line.strip().scanf("Operation: new = old $c $i", operator, rhsInt):
      monkeys[monkeys.high()].operator = operator
      monkeys[monkeys.high()].rhs = $rhsInt
      continue  
    
    # The test for divisibility  
    if line.strip().scanf("Test: divisible by $i", divisor):
      monkeys[monkeys.high()].divisor = divisor
      continue
    
    # The true condition
    if line.strip().scanf("If true: throw to monkey $i", receiver):
      monkeys[monkeys.high()].trueRecv = receiver
    
    # The false condition  
    if line.strip().scanf("If false: throw to monkey $i", receiver):
      monkeys[monkeys.high()].falseRecv = receiver
  
  # Ensure objects are in sequence
  monkeys.sort(proc(x, y: Monkey): int = cmp(x.id, y.id))

# Processes passing of the items between the monkeys according to the rules
# defined in the problem. updateLevel is a function that will provide the new
# value of the worry level for each item.
proc processRounds(rounds: int, updateLevel: proc(oldLevel: int): int): 
  seq[Monkey] =
  
  result = monkeys
 
  for round in 1 .. rounds:
    for monkeyIdx in result.low() .. result.high():
      result[monkeyIdx].inspectionCount.inc(result[monkeyIdx].items.len())
      
      # Distribute the items
      for item in result[monkeyIdx].items:
        var
          newWorryLevel, rhs: int
        
        case result[monkeyIdx].rhs:
          of "old":
            rhs = item
          else:
            rhs = result[monkeyIdx].rhs.parseInt()
        
        case result[monkeyIdx].operator
          of '+':
            newWorryLevel = item + rhs
          of '-':
            newWorryLevel = item - rhs
          of '*':
            newWorryLevel = item * rhs
          of '/':
            newWorryLevel = item div rhs
          else:
            continue  
        
        # calculate the new level using the update function
        newWorryLevel = newWorryLevel.updateLevel()
        
        # Decide where to send the item
        if newWorryLevel mod result[monkeyIdx].divisor == 0:
          result[result[monkeyIdx].trueRecv].items.add(newWorryLevel)
        else:
          result[result[monkeyIdx].falseRecv].items.add(newWorryLevel)
      
      # Empty the list of items
      result[monkeyIdx].items = @[] 

# Level of monkey business after 20 rounds. The worry level for each item 
# undergoes integer division by 3 after inspection.
block part1:          
  let counts = processRounds(20, proc(oldLevel: int): int = 
    oldLevel.floorDiv(3)).map(proc(x: Monkey): int = 
    x.inspectionCount).sorted(SortOrder.Descending)
  
  echo "Part 1: ", counts[0] * counts[1]

# Level of monkey business after 10000 rounds.
# Here, to keep the worry level in check, we can set it to the remainder after 
# dividing it by the LCM of all the divisors. This works, because the test for 
# divisibility will give the same result on the remainder.
# https://www.reddit.com/r/adventofcode/comments/zifqmh/2022_day_11_solutions/
# https://github.com/bereal/AdventOfCodeHaskell/blob/main/src/Year2022/Day11.hs  
block part2:
  var
    lcm = monkeys.map(proc(x: Monkey): int = x.divisor).lcm()
          
  let counts = processRounds(10000, proc(oldLevel: int): int = oldLevel mod lcm)
    .map(proc(x: Monkey): int = x.inspectionCount).sorted(SortOrder.Descending)
  
  echo "Part 2: ", counts[0] * counts[1]
