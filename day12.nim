# https://adventofcode.com/2022/day/12

import os, strutils, sequtils, sugar

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  Point = tuple[row, col: int]

var
  map: seq[seq[char]] = @[]
  start, finish: Point

# Read the file and build a map of the area
block readInput:
  for line in readFile(os.paramStr(1)).strip().splitLines():
    var
      idx: int
    
    # Add the contents of the line
    map.add(line.strip().toSeq())
    
    # Set start and finish
    idx = map[map.high()].find('S')
    if idx >= 0:
      start = (row: map.high(), col: idx)
    
    idx = map[map.high()].find('E')
    if idx >= 0:
      finish = (row: map.high(), col: idx)

# Gets the level of a point
proc getLevel(point: Point): int =
  case map[point.row][point.col]
    of 'S':
      return 1
    of 'E':
      return 26
    else:
      return map[point.row][point.col].ord() - 96 # ord('a') - 1

# Here only the minimum path length is needed, so there is no need to explore
# every path. The step when the destination is first reached is required.

# Based on https://github.com/Fadi88/AoC/blob/master/2022/day12/code.py 
# Other approaches can be found in the subreddit
# https://www.reddit.com/r/adventofcode/comments/zjnruc/2022_day_12_solutions/

# Gets the minimum number of steps in which the destination is reached from any 
# of the specified starting points
proc getMinimumSteps(sources: seq[Point], destination: Point): int =
  result = 0
  
  var
    next: seq[seq[Point]] = @[sources]
    visited: seq[seq[bool]] = @[]
  
  # Nothing is visited initially
  for row in map:
    visited.add(repeat(false, row.len()))
  
  # next[next.low()] is the list of points to be visited in the current pass
  while next.len() > 0:
    if next[next.low()].len() == 0:
      break
    
    # The list of points to be visited in the next pass
    next.add(@[])
    
    # Process the current list of points
    for point in next[next.low()]:      
      # For optimum path, each point must be visited only once
      if visited[point.row][point.col]:
        continue
      
      # Keep track of visited points to avoid loops
      visited[point.row][point.col] = true
      
      # Check if destination is reached
      if point == destination:
        return
      
      # Check the adjacent points
      for dRow in -1 .. 1:
        for dCol in -1 .. 1:
          # Exclude the current and diagonally adjacent points
          if (dRow == 0 and dCol == 0) or (dRow != 0 and dCol != 0):
            continue
          
          # Calculate the row and column
          var
            nextRow, nextCol: int
          
          nextRow = point.row + dRow
          if nextRow < map.low() or nextRow > map.high():
            continue
          
          nextCol = point.col + dCol
          if nextCol < map[nextRow].low() or nextCol > map[nextRow].high():
            continue
          
          # Compare levels and decide whether to visit the point next
          var
            nextLevel, currentLevel: int
            nextPoint: Point
          
          nextPoint = (row: nextRow, col: nextCol)
          currentLevel = point.getLevel()
          nextLevel = nextPoint.getLevel()
          
          if (nextLevel - currentLevel == 1) or (nextLevel <= currentLevel):  
            next[next.high()].add(nextPoint)
    
    # Increment step counter  
    result.inc()
    
    # Remove list of processed points
    next.delete(next.low())  

# Minimum steps from start to finish
block part1:       
  echo "Part 1: ", @[start].getMinimumSteps(finish)

# Minimum steps from any point at same level as start ('a') to finish
block part2:
  # Get all eligible starting points
  let startPoints = collect(newSeq):
    for row in map.low() .. map.high():
      for col in map[row].low() .. map[row].high():
        var
          point: Point
        
        point = (row: row, col: col)
        if point.getLevel() != 1:
          continue
          
        point
  
  echo "Part 2: ", startPoints.getMinimumSteps(finish)
