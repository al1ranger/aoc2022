# https://adventofcode.com/2022/day/25

import os, strutils, sequtils

# File must be provided
if os.paramCount() < 1:
  quit(1)

let numbers = readFile(os.paramStr(1)).strip().splitLines()

# Here, the important thing to understand is that the SNAFU representation must
# contain only 0, 1, 2, = and -. This means the conversion has to be done 
# differently as compared to other base N conversions. Approaches can be found 
# in the subreddit
# https://www.reddit.com/r/adventofcode/comments/zur1an/2022_day_25_solutions/

# Convert SNAFU to decimal
proc snafuToDec(input: string): int64 =
  result = 0
  
  var
    multiplier: int64 = 1
  
  # Calculate integer values from right to left, taking the correct multiplier
  for idx in countdown(input.high(), input.low()):
    case input[idx]
      of '-': # -1
        result -= multiplier
      of '=': # -2
        result -= (2 * multiplier)
      else:
        try:
          result += (parseInt($input[idx]) * multiplier)
        except ValueError:
          discard
      
    multiplier *= 5

# Convert decimal to SNAFU
proc decToSnafu(input: int64): string =
  result = ""
  
  var
    value: int64 = input
    digit: int64
  
  while value > 0:
    digit = value mod 5
    # Here, the digit is expressed as follows
    # 0, 1, 2 -> No change
    # 3 -> 5 - 2, i.e. add 5 to value so that this can be represented as =
    # 4 -> 5 - 1, i.e. add 5 to value so that this can be represented as - 
    case digit
      of 3: # = 5 - 2 => add 5 to value and output =
        value += 5
        result = '=' & result
      of 4: # = 5 - 1 => add 5 to value and output -
        value += 5
        result = '-' & result
      else:
        result = $digit & result
    
    value = value div 5
  
block part1:
  echo "Part 1: ", numbers.foldl(a + b.snafuToDec(), int64(0)).decToSnafu()
