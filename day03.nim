# https://adventofcode.com/2022/day/3

import os, strutils, sets, sequtils, math

# File must be provided
if os.paramCount() < 1:
  quit(1)

# Read the data
let data = readFile(os.paramStr(1)).strip().splitLines()

# Calclate the priority of an item
proc calculatePriority(item: char): int =
  result = 0
  
  # In ASCII/Unicode, the code for 'A' is 65 and that for 'Z' is 90
  # Similarly 'a' is 97 and 'z' is 125
  # Nim's ord function will return this value
  case item:
    of 'a' .. 'z':
      result = ord(item) - 96
    of 'A' .. 'Z':
      result = ord(item) - 38 # ord(item) - 64 + 26
    else:
      discard

# Calculate total priority of items that appear in both compartments
block part1:    
  # Use mapIt and sum to calculate the result
  # Use set intersection to find the common item in both the compartments
  echo "Part 1: ", data.mapIt(toHashSet(it[0 ..< it.len() div 2])
    .intersection(toHashSet(it[it.len() div 2 ..< it.len()]))
    .toSeq()[0]
    .calculatePriority()).sum()

# Calculate total priority of items that appear in a group of three
block part2:
  # Process the rucksack contents in groups of three
  # Build a sequence of the start indices of the data and then use the 
  # foldl template for the sum, using set intersection to find the common item      
  echo "Part 2: ", foldl(countup(0, data.len() - 1, 3).toSeq(), 
    a + toHashSet(data[b])
      .intersection(toHashSet(data[b + 1]))
      .intersection(toHashSet(data[b + 2]))
      .toSeq()[0]
      .calculatePriority(),
    0)
