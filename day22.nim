# https://adventofcode.com/2022/day/22

import os, strutils

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  Position = tuple[row, col: int, facing: int]
  MoveCommand = tuple[steps: int, direction:char]
  
var
  map: seq[string] = @[]
  commands: seq[MoveCommand] = @[]

block readInput:
  var
    mapRead: bool = false
  
  for line in readFile(os.paramStr(1)).splitLines():   
    if line.len() == 0:
      mapRead = true
      continue
    
    if mapRead:
      var
        steps: string
      
      steps = ""
      for c in line.strip():
        if c == 'L' or c == 'R':
          commands.add((steps: steps.parseInt(), direction: c))
          steps = ""
          continue
        
        steps &= c
      
      commands.add((steps: steps.parseInt(), direction: ' '))
      
      break
    
    map.add(line)

# Calculate position for wrapped movements
block part1:  
  # Calculate the next position based on direction faced. Returns the updated 
  # position based on the movement.
  proc getNextPosition(map: seq[string], start: Position): Position =
    result = start
    
    var
      destRow, destCol: int
    
    destRow = start.row
    destCol = start.col
    
    # Locate the next valid destination
    case result.facing
      of 0:
        destRow = start.row
        destCol = start.col + 1
        
        if destCol > map[destRow].high() or map[destRow][destCol] == ' ':
          for col in map[destRow].low() .. map[destRow].high:
            if map[destRow][col] == ' ':
              continue
              
            if map[destRow][col] == '.' or map[destRow][col] == '#':
              destCol = col
              break
      of 2:
        destRow = start.row
        destCol = start.col - 1
        
        if destCol < map[destRow].low() or map[destRow][destCol] == ' ':
          for col in countdown(map[destRow].high(), map[destRow].low()):
            if map[destRow][col] == ' ':
              continue
              
            if map[destRow][col] == '.' or map[destRow][col] == '#':
              destCol = col
              break
      of 3:
        destRow = start.row - 1
        destCol = start.col
        
        if destRow < map.low() or destCol > map[destRow].high() or
          map[destRow][destCol] == ' ':
          for row in countdown(map.high(), map.low()):
            if map[row].high() < destCol or map[row][destCol] == ' ':
              continue
            
            if map[row][destCol] == '.' or map[row][destCol] == '#':
              destRow = row
              break
              
      of 1:
        destRow = start.row + 1
        destCol = start.col
        
        if destRow > map.high() or destCol > map[destRow].high() or 
          map[destRow][destCol] == ' ':
          for row in map.low() .. map.high():
            if map[row].high() < destCol or map[row][destCol] == ' ':
              continue
            
            if map[row][destCol] == '.' or map[row][destCol] == '#':
              destRow = row
              break
      else:
        discard
    
    # Do not update if next destination is a wall
    if map[destRow][destCol] == '#':
      return
    
    result.row = destRow
    result.col = destCol
  
  # Moves the specified number of steps
  proc move(map: seq[string], start: Position, steps: int): Position =
    result = start
    
    # Nothing to do
    if steps < 1:
      return
    
    # Get next position
    result = map.getNextPosition(start)
    
    # No movement
    if result == start:
      return
    
    # Perform the remaining movement
    result = map.move(result, steps - 1)  
  
  # Rotates depending on the current orientation and the turn direction
  proc rotate(map: seq[string], start: Position, direction: char): Position =
    result = start
    
    # Because the directions are numbered 0, 1, 2, 3 staring from right and 
    # proceeding clockwise, left and right turns can be calculated by moving in
    # a circular fashion (i.e. 0 - 1 -> 3 and 3 + 1 -> 0)
    
    case direction
      of 'L':
        result.facing = start.facing - 1
        if result.facing < 0:
          result.facing = 3
      of 'R':
        result.facing = start.facing + 1
        if result.facing > 3:
          result.facing = 0
      else:
        discard

  var
    current: Position = (row: 0, col: map[0].find('.'), facing: 0)
  
  for command in commands:
    current = map.rotate(map.move(current, command.steps), command.direction)
  
  echo "Part 1: ", 1000 * (current.row + 1) + 4 * (current.col + 1) + 
    current.facing  
  
# To be implemented
block part2:
  discard
