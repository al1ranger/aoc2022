# https://adventofcode.com/2022/day/24

import os, strutils, sets

# File must be provided
if os.paramCount() < 1:
  quit(1)

# We need to process all possible positions for a given turn at the same time as
# the available positions change due to the movements of the blizzards. 
# The subreddit provides some hints on possible optimizations to keep memory 
# requirements within reasonable limits while searching the optimum path
# https://www.reddit.com/r/adventofcode/comments/zu28ij/2022_day_24_solutions/
# Based on https://gist.github.com/royvanrijn/3dd85135c8ae478fe0d8b41bb5b26067

type
  Point = tuple[row, col: int]

var
  valley: tuple[rows, cols: int] = ((rows: -1, cols: 0))
  blizzards: seq[Point]= @[] # Directions separate so that contains can be used
  blizzardsD: seq[int] = @[] # 0 => >, 1 => v, 2 => <, 3 => ^
  start, finish: Point

# Read blizzard positions and start and finish
block readInput:
  const
    Directions = ['>', 'v', '<', '^']
  
  for line in readFile(os.paramStr(1)).strip().splitLines():
    valley.rows.inc()
    valley.cols = line.high()
    
    # Probably not required as start seems to be the second character on the 
    # first line and finish is the second last character on the last line. 
    # When this is not the case, the below logic will handle it.
    if valley.rows == 0:
      start = ((row: valley.rows, col: line.find('.')))
    else:
      finish = ((row: valley.rows, col: line.find('.')))
    
    for i in line.low() .. line.high():      
      # Check if there is a blizzard at this location
      if line[i] == '#' or line[i] == '.':
        continue
      
      blizzards.add((row: valley.rows, col: i))
      blizzardsD.add(Directions.find(line[i]))

# Calculate the shortest time required to go from start to finish. Takes an 
# initial list of blizzard positions. Updates the list of blizzard positions 
# and returns the time required.
proc calculateShortestTime(start: Point, finish:Point, 
  blizzardPositions: var seq[Point]): int =
  result = 1
  
  var
    positionsToCheck: HashSet[Point] = @[start].toHashSet()
    newPositions: HashSet[Point]
    found: bool = false
    
  while not found:
    # Update the blizzard positions
    for idx in blizzardPositions.low() .. blizzardPositions.high():
      case blizzardsD[idx]
        of 0: # Right
          blizzardPositions[idx].col.inc()
        of 1: # Down
          blizzardPositions[idx].row.inc()
        of 2: # Left
          blizzardPositions[idx].col.dec()
        of 3: # Up
          blizzardPositions[idx].row.dec()
        else:
          discard
      
      # Adjust co-ordinates as needed
      if blizzardPositions[idx].row == valley.rows:
        blizzardPositions[idx].row = 1
        
      if blizzardPositions[idx].row == 0:
        blizzardPositions[idx].row = valley.rows - 1
      
      if blizzardPositions[idx].col == valley.cols:
        blizzardPositions[idx].col = 1
      
      if blizzardPositions[idx].col == 0:
        blizzardPositions[idx].col = valley.cols - 1
    
    block eachTurn:
      # Examine the possible positions for this turn and generate options for 
      # the next
      newPositions = initHashSet[Point]()
      while positionsToCheck.len() > 0:
        var
          currentPosition, newPosition: Point
        
        currentPosition = positionsToCheck.pop()
        
        # Check the four directions, excluding diagonal
        for dRow in -1 .. 1:
          for dCol in -1 .. 1:
            #    -1  0  1
            # -1  X  N  X
            #  0  W  X  E
            #  1  X  S  X
            if abs(dRow) == abs(dCol):
              continue
            
            newPosition.row = currentPosition.row + dRow
            newPosition.col = currentPosition.col + dCol
            
            # Check whether destination is reached
            if newPosition == finish:
              newPositions.incl(newPosition)
              break eachTurn
            
            # Check for invalid position
            if newPosition.row <= 0 or newPosition.row >= valley.rows or 
              newPosition.col <= 0 or newPosition.col >= valley.cols:
              continue
            
            # Check whether there is a blizzard at this position
            if blizzardPositions.contains(newPosition):
              continue
            
            # Check this next time 
            newPositions.incl(newPosition)
        
        # No movement is an option only if there is no blizzard at the current 
        # position
        if blizzardPositions.contains(currentPosition):
          continue
        newPositions.incl(currentPosition)
    
    # Check whether next pass is required
    if newPositions.contains(finish):
      found = true
      continue
    
    # Prepare for next pass
    positionsToCheck = newPositions
    result.inc()
  
var
  blizzardPositions: seq[Point] = blizzards  
  time: int = 0    

# Start to finish in the shortest time      
block part1:
  time = start.calculateShortestTime(finish, blizzardPositions)
  echo "Part 1: ", time

# Start to finish (Part 1), back to start and again to the finish
block part2:
  time.inc(finish.calculateShortestTime(start, blizzardPositions))
  time.inc(start.calculateShortestTime(finish, blizzardPositions))
  echo "Part 2: ", time
