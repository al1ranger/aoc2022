# https://adventofcode.com/2022/day/2

# The order is Rock (1), Paper (2) and Scissors (3).
# The winning choice is the one after the current in a circular list.
# i.e. Rock -> Paper -> Scissors -> Rock
# The losing choice is the one before the current in a circular list.
# i.e. Scissors <- Rock <- Paper <- Scissors  

import os, strutils

# File must be provided
if os.paramCount() < 1:
  quit(1)

# Read the data
let data = readFile(os.paramStr(1)).strip().splitLines()

# Data is in the format of player and opponent choices
block part1:
  # Calculate the score of the choice
  proc getChoiceScore(choice: char): int =
    case choice:
      of 'A', 'X': # Rock
        result = 1
      of 'B', 'Y': # Paper
        result = 2
      of 'C', 'Z': # Scissors
        result = 3
      else:
        result = 0
  
  # Calculate the player's score in the current turn
  proc getTurnScore(playerChoice: char, opponentChoice: char): int =
    # Get score of player's choice    
    result = getChoiceScore(playerChoice)
    
    # Determine win, loss or draw using the score difference
    # Here, getChoiceScore(playerChoice) - getChoiceScore(opponentChoice)
    # For a win, it will be 1 or -2
    # For a draw, it will be 0
    # For a loss, it will be -1 or 2
    case (result - getChoiceScore(opponentChoice)):
      of 1, -2: # Win
        result += 6
      of 0: # Draw
        result += 3
      else:
        discard
  
  var
    totalScore: int = 0
    playerIdx: int = 2
    opponentIdx: int = 0
  
  for line in data:
    totalScore += getTurnScore(line[playerIdx], line[opponentIdx])
    
    # Calculate the indexes for the next pass
    opponentIdx = 2 - playerIdx
    playerIdx = 2 - opponentIdx
    
  echo "Part 1: ", totalScore

# Data is in the form of opponent choice and outcome
block part2:
  proc getTurnScore(opponentChoice: char, outcome: char): int =    
    var
      delta: int = 0
      choiceScore: int = 0
    
    case outcome:
      of 'X': # Loss, player needs to select previous item in list
        delta = -1 
      of 'Y': # Draw, player selects same item as opponent
        result += 3 # 3 points for a draw
        delta = 0
      of 'Z': # Win, Player selects next item in list
        result += 6 # 6 points for a win
        delta = 1
      else:
        discard
    
    # Calculate the score of the player's choice
    case opponentChoice:
      of 'A': # Rock
        choiceScore = 1 + delta
      of 'B': # Paper
        choiceScore = 2 + delta
      of 'C': # Scissors
        choiceScore = 3 + delta
      else:
        discard
    
    if choiceScore < 1: # Going in a circle, scissors come before rock
      choiceScore = 3
    
    if choiceScore > 3: # Going in a circle, rock comes after scissors
      choiceScore = 1
    
    # Add the score of the player's choice
    result += choiceScore

  var
    totalScore: int = 0
  
  for line in data:
    # Update score depending on the opponent's shape and outcome
    totalScore += getTurnScore(line[0], line[2])

  echo "Part 2: ", totalScore
