# https://adventofcode.com/2022/day/23

import os, strutils, tables

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  Point = tuple[x, y: int]

var
  elves: seq[Point] = @[]
  
block readInput:
  let
    data = readFile(os.paramStr(1)).strip().splitLines()
    
  for row in countdown(data.high(), data.low()):
    for col in data[row].low() .. data[row].high:
      if data[row][col] == '#':
        elves.add((x: col, y: data.high() - row))


type
  Direction = enum
    southwest, south, southeast, west, none, east, northwest, north, northeast

# Returns a list of directions containing an elf adjacent to the input
proc adjacentElves(elves: seq[Point], point: Point): seq[Direction] =
  result = @[]
  
  # The directions are as below for values od dx and dy
  #    -1     0    1
  #  1 NW(6) N(7) NE(8)
  #  0  W(3)  (4)  E(5) 
  # -1 SW(0) S(1) SE(2)
  # These can be mapped to the range 0 to 8 using the formula 
  # 3 * (dy + 1) + dx + 1
  for dx in -1 .. 1:
    for dy in -1 .. 1:
      if dx == 0 and dy == 0:
        continue
      
      if elves.contains((x: point.x + dx, y: point.y + dy)):
        result.add(Direction(3 * dy + dx + 4)) #Directions[dy + 1][dx + 1])

# Gets the next position depending on the current position and starting rule
proc getNextPosition(elves: seq[Point], current: Point, startruleIdx: int): Point =
  result = current
  
  var
    adjacent: seq[Direction]
    ruleIdx: int
  
  # Get directions of adjacent elves
  adjacent = elves.adjacentElves(current)
  
  # No adjacent elves, so no movement
  if adjacent.len() == 0:
    return
  
  # Cycle through the rules till one matches
  ruleIdx = startruleIdx
  for delta in 0 .. 3:            
    case ruleIdx
      of 0:
        if not adjacent.contains(north) and not adjacent.contains(northeast) and 
          not adjacent.contains(northwest):
          return ((x: current.x, y: current.y + 1))
      of 1:
        if not adjacent.contains(south) and not adjacent.contains(southeast) and 
          not adjacent.contains(southwest):
          return ((x: current.x, y: current.y - 1))
      of 2:
        if not adjacent.contains(west) and not adjacent.contains(northwest) and 
          not adjacent.contains(southwest):
          return ((x: current.x - 1, y: current.y))
      of 3:
        if not adjacent.contains(east) and not adjacent.contains(northeast) and 
          not adjacent.contains(southeast):
          return ((x: current.x + 1, y: current.y))
      else:
        discard
    
    # Move to the next rule
    ruleIdx.inc()
    if ruleIdx > 3:
      ruleIdx = 0

# Executes a turn of movement
proc executeTurn(currentPositions: seq[Point], startingRule: int): seq[Point] =
  result = @[]
  
  var
    elfSelections: CountTable[Point]
  
  # First half: Execute the movements for all elves
  for elf in currentPositions:
    result.add(currentPositions.getNextPosition(elf, startingRule))
    
    # Keep track of the proposed movements
    if not elfSelections.hasKey(result[result.high()]):
      elfSelections[result[result.high()]] = 0
    elfSelections.inc(result[result.high()])
  
  # Second half: Finalize change depending on how many elves proposed the same
  # destination  
  for elfIdx in result.low() .. result.high():
    if elfSelections[result[elfIdx]] > 1:
      result[elfIdx] = currentPositions[elfIdx]  

var
  currentPositions: seq[Point] = @[]
  newPositions: seq[Point] = elves
  startingRule: int = 0 # 0 -> N, 1 -> S, 2 -> W, 3 -> E
  turns: int

# Empty ground tiles after 10 turns
block part1:  
  # Execute turn turns, cycling through the starting rule at each turn
  for turn in 1 .. 10: 
    currentPositions = newPositions   
    newPositions = currentPositions.executeTurn(startingRule)
    
    # Update starting rule    
    startingRule.inc()
    if startingRule > 3:
      startingRule = 0
    
    turns = turn
  
  # Calculate the minimum and maximum co-ordinates
  var
    minX, maxX, minY, maxY: int
  
  minX = int.high
  maxX = int.low
  minY = int.high
  maxY = int.low
  
  for position in newPositions:
    if position.x < minX:
      minX = position.x
    
    if position.x > maxX:
      maxX = position.x
    
    if position.y < minY:
      minY = position.y
    
    if position.y > maxY:
      maxY = position.y
  
  # As each co-ordinate refers to a tile, the value 0 is also a tile
  echo "Part 1: ", (maxX - minX + 1) * (maxY - minY + 1) - 
    currentPositions.len()

# First turn at which no elf moves
block part2:
  var
    movement: bool = true
  
  # Run till there is movement
  while movement:    
    # Calculate the new positions
    currentPositions = newPositions
    newPositions = currentPositions.executeTurn(startingRule) 
    
    # Increment turn counter
    turns.inc()
    
    # Check whether any movement occurred
    movement = false
    for elfIdx in newPositions.low() .. newPositions.high():
      if newPositions[elfIdx] != currentPositions[elfIdx]:
        movement = true
        break
    
    # Update starting rule   
    startingRule.inc()
    if startingRule > 3:
      startingRule = 0
  
  echo "Part 2: ", turns
