# https://adventofcode.com/2022/day/15

import os, strutils, strscans, sets, algorithm

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  Point = tuple[x, y: int]
  Reading = tuple[sensor, beacon: Point]

var 
  readings: seq[Reading] = @[]
  row, gridsize: int = 0

# Row to be used for part 1
if os.paramCount() >= 2:
  row = os.paramStr(2).parseInt()
else:
  row = 2000000

# Grid size for part 2  
if os.paramCount() >= 3:
  gridSize = os.paramStr(3).parseInt()
else:
  gridSize = 4000000

# Extract details of scanners and beacons
block readInput:
  for line in readFile(os.paramStr(1)).strip().splitLines():
    var
      sensorX, sensorY, beaconX, beaconY: int
    
    if line.scanf("Sensor at x=$i, y=$i: closest beacon is at x=$i, y=$i",
      sensorX, sensorY, beaconX, beaconY):
      readings.add((sensor: (x: sensorX, y: sensorY), 
        beacon: (x: beaconX, y:beaconY)))

# Here columns indicate the X-axis and rows indicate the Y-axis.

# Calculate number of positions in a row where beacon cannot be present.
# Because this is only for a single row, a brute force approach of building a 
# list of excluded points will give acceptable performance. We can also use an
# approach similar to part 2 to speed up the calculation.   
block part1:
  var
    excludedPositions: HashSet[Point]
    
  excludedPositions.init()
  
  for reading in readings:
    var
      dist, dy, distX: int
    
    # Calculate Manhattan distance of the sensor
    dist = abs(reading.sensor.x - reading.beacon.x) + 
      abs(reading.sensor.y - reading.beacon.y)
    
    # Check whether the sensor can actually read the row in question
    dy = row - reading.sensor.y
    if abs(dy) > dist:
      continue
    
    # Exclude the range of X-cordinates for this sensor
    distX = abs(dist - abs(dy))
    for dx in -distX .. distX:            
      excludedPositions.incl((x: reading.sensor.x + dx, 
        y: reading.sensor.y + dy))
  
  # Exclude the beacons
  for reading in readings:
    excludedPositions.excl(reading.beacon)

  echo "Part 1:", excludedPositions.len()

# Locate the distress beacon.
# Here a brute force approach won't work due to the size of the grid. It is 
# much faster to calculate the intersections of the X-axis ranges of all the 
# scanners for each row on the Y-axis. The row containing the beacon will have 
# exactly one value in the X direction that is not covered by any scanner. 
# The subreddit provides some ideas for achieving this
# https://www.reddit.com/r/adventofcode/comments/zmcn64/2022_day_15_solutions/
block part2:  
  var
    signalRow, signalColumn: int
    
  block eval:
    for row in countdown(gridSize, 0):         
      var
        ranges: seq[tuple[min, max: int]]
      
      ranges = @[]
      
      for reading in readings:
        var
          dist, dy, distX: int
        
        # Calculate Manhattan distance of the sensor
        dist = abs(reading.sensor.x - reading.beacon.x) + 
          abs(reading.sensor.y - reading.beacon.y)
        
        # Check whether the sensor can actually read the row in question
        dy = row - reading.sensor.y
        if abs(dy) > dist:
          continue
        
        # Store the range along the X-axis that is covered by the sensor
        distX = abs(dist - abs(dy))
        ranges.add((min: max(reading.sensor.x - distX, 0), 
          max: min(reading.sensor.x + distX, gridSize)))
      
      # Sort ranges in increasing order of their minimum X co-ordinate
      ranges.sort do (x, y: tuple[min, max: int]) -> int:
        result = cmp(x.min, y.min)
      
      # Examine the ranges for a gap
      signalColumn = 0
      for scannerRange in ranges:
        # Can the scanner detect this X co-ordinate?
        if scannerRange.min <= signalColumn:
          # If column is covered by range of current scanner, set the column to 
          # be immediately outside of this scanner's range. IF there is a gap,
          # the check will fail for the next scanner
          if scannerRange.max >= signalColumn:
            signalColumn = scannerRange.max + 1
          continue
        
        # Scanner could not detect the column. We've found the gap.
        signalRow = row
        break eval
  
  echo "Part 2:", signalRow + 4000000 * signalColumn
