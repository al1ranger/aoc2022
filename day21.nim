# https://adventofcode.com/2022/day/21

import os, strutils, tables, strscans

# File must be provided
if os.paramCount() < 1:
  quit(1)

type
  Task = tuple[lhs, rhs: string, operator: char]

var
  monkeyJobs: Table[string, Task]

# Build a map of the task assigned to each monkey  
block readInput:
  for line in readFile(os.paramStr(1)).strip().splitLines():
    var
      worker: string
      task: Task
      lhs: int
    
    if line.strip().scanf("$w: $i", worker, lhs):
      task.lhs = $lhs
      task.rhs = " "
      task.operator = ' '
      monkeyJobs[worker] = task
      continue
    
    if line.strip().scanf("$w: $w $c $w", worker, task.lhs, task.operator, task.rhs):
      monkeyJobs[worker] = task

# Evaluate what root will speak
block part1:  
  # Evaluates the task for a given monkey and return the value
  proc evaluateTask(jobs: Table[string, Task], worker: string): int =
    result = 0
    
    var
      lhs, rhs: int = 0
    
    if not jobs.hasKey(worker):
      return
    
    # Get the LHS value
    try:
      lhs = jobs[worker].lhs.parseInt()
    except ValueError:
      lhs = jobs.evaluateTask(jobs[worker].lhs)
    
    # Get the RHS value
    if jobs[worker].rhs.len() > 0:
      try:
        rhs = jobs[worker].rhs.parseInt()
      except ValueError:
        rhs = jobs.evaluateTask(jobs[worker].rhs)
    
    # Calculate depending on the operation
    case jobs[worker].operator
      of '+':
        result = lhs + rhs
      of '-':
        result = lhs - rhs
      of '*':
        result = lhs * rhs
      of '/':
        result = lhs div rhs
      else:
        result = lhs

  echo "Part 1: ", monkeyJobs.evaluateTask("root")

# Evaluate what humn has to say
block part2:
  # Evaluates the task for a given monkey and return the value, or 'humn' if the 
  # value depends on it
  proc evaluateTask(jobs: Table[string, Task], worker: string): string =
    result = ""
    
    if not jobs.hasKey(worker):
      return
    
    # For humn always return humn
    if worker == "humn":
      return "humn"
    
    var
      lhs, rhs: string = ""
    
    # Evaluate LHS
    try:
      lhs = $(jobs[worker].lhs.parseInt())
    except ValueError:
      lhs = jobs.evaluateTask(jobs[worker].lhs)
    
    # Evaluate RHS
    if jobs[worker].rhs.len() > 0:
      try:
        rhs = $(jobs[worker].rhs.parseInt())
      except ValueError:
        rhs = jobs.evaluateTask(jobs[worker].rhs)
    
    # If either value is humn, then result cannot be evaluated
    if lhs == "humn" or rhs == "humn":
      return "humn"
    
    # Calculate the value as there is no dependency on humn
    case jobs[worker].operator
      of '+':
        result = $(lhs.parseInt() + rhs.parseInt())
      of '-':
        result = $(lhs.parseInt() - rhs.parseInt())
      of '*':
        result = $(lhs.parseInt() * rhs.parseInt())
      of '/':
        result = $(lhs.parseInt() div rhs.parseInt())
      else:
        result = lhs
  
  # Pushes down the expected result to the lower level and returns the 
  # required value for humn
  proc pushDownToHumn(jobs: Table[string, Task], worker: string, 
    value: int = 0): int =
    
    if not jobs.hasKey(worker):
      return 0
    
    # This is the final target, so value is the one needed
    if worker == "humn":
      return value
      
    var
      lhs, rhs, target: string
      nextValue: int
    
    # Evaluate LHS
    try:
      lhs = $(jobs[worker].lhs.parseInt())
    except ValueError:
      lhs = jobs.evaluateTask(jobs[worker].lhs)
    
    # Evaluate RHS
    try:
      rhs = $(jobs[worker].rhs.parseInt())
    except ValueError:
      rhs = jobs.evaluateTask(jobs[worker].rhs)  
    
    # For the worker, the equation is value = lhs operation rhs
    # So work out target and value for humn that needs to be pushed down.
    # For root, the operation is equality, which gives us the starting value.
    target = "" 
    if lhs == "humn":
      if worker == "root":
        nextValue = rhs.parseInt()
      else:  
        case jobs[worker].operator
          of '+':
            nextValue = value - rhs.parseInt()
          of '-':
            nextValue = value + rhs.parseInt()
          of '*':
            nextValue = value div rhs.parseInt()
          of '/':
            nextValue = value * rhs.parseInt()
          else:
            nextvalue = 0
      
      target = jobs[worker].lhs  
      
    if rhs == "humn":
      if worker == "root":
        nextValue = lhs.parseInt()
      else:
        case jobs[worker].operator
          of '+':
            nextValue = value - lhs.parseInt()
          of '-':
            nextValue = lhs.parseInt() - value
          of '*':
            nextValue = value div lhs.parseInt()
          of '/':
            nextValue = lhs.parseInt() div value
          else:
            nextvalue = 0
      
      target = jobs[worker].rhs 
      
    return jobs.pushDownToHumn(target, nextValue)
      
  echo "Part 2: ", monkeyJobs.pushDownToHumn("root")
