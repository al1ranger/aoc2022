# https://adventofcode.com/2022/day/1

import os, strutils, algorithm

# File must be provided
if os.paramCount() < 1:
  quit(1)

# Read the data
let data = readFile(os.paramStr(1)).strip().splitLines()

var
  elfCalories: seq[int] = @[]

# Calculate the calories
block calculateCalories:
  var
    calories: int
  
  for line in data:
    # An empty line means that data for the current elf is over
    if line.len() == 0:
      elfCalories.add(calories)
      calories = 0
      continue
    
    # Add the calories  
    calories = calories + parseInt(line)
  
  # The last elf
  elfCalories.add(calories)  
  
  # Sort in descending order of calories
  elfCalories.sort(SortOrder.Descending)

block part1: # The elf with maximum calories
  echo "Part 1: ", elfCalories[0]

block part2: # Sum of the top three calorie values
  echo "Part 2: ", elfCalories[0] + elfCalories[1] + elfCalories[2]
