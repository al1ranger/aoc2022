# https://adventofcode.com/2022/day/4

import os, strutils, strscans, sequtils

# File must be provided
if os.paramCount() < 1:
  quit(1)

# Read the data
let data = readFile(os.paramStr(1)).strip().splitLines()

# Check for complete overlap
block part1:
  # Check if one region is completely contained in another. This is a 
  # straightforward comparison of the start and end values of the region.
  # This can be seen below.
  #     ┌----┌----┐-----┐
  #     |    |    |     |
  #     |    |    |     |
  #     └----└----┘-----┘
  #    s1    s2   e2    e1
  proc isOverlapping(input: string): int =
    var
      start1, start2, end1, end2: int
    
    result = 0
    
    if not input.scanf("$i-$i,$i-$i", start1, end1, start2, end2):
      return
    
    if (start1 <= start2 and end1 >= end2) or
      (start2 <= start1 and end2 >= end1):
      result = 1
    else:
      result = 0
  
  # Use the foldl template to calculate the overlap for each line in input
  echo "Part 1 : ", data.foldl(a + b.isOverlapping(), 0)

# Check for partial or complete overlap
block part2:
  # Check if one region is partially or fully contained in another. 
  # Here, we need to check start of one region with the end of the other. 
  # This can be seen below.
  #     ┌----┌----┐-----┐       ┌----┌----┐-----┐
  #     |    |    |     |       |    |    |     |
  #     |    |    |     |       |    |    |     |
  #     └----└----┘-----┘       └----└----┘-----┘
  #    s1    s2   e1    e2      s1   s2   e2    e1
  proc isOverlapping(input: string): int =
    var
      start1, start2, end1, end2: int
    
    result = 0
    
    if not input.scanf("$i-$i,$i-$i", start1, end1, start2, end2):
      return
  
    if (start1 <= end2 and end1 >= start2) or
      (start2 <= end1 and end2 >= start1):
        result = 1
    else:
      result = 0
  
  # Use the foldl template to calculate the overlap for each line in input
  echo "Part 2 : ", data.foldl(a + b.isOverlapping(), 0)
