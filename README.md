# Advent of Code 2022 Solutions

## About
This repository contains the solutions for the puzzles in the [2022 Advent of Code calendar](https://adventofcode.com/2022).

The solutions are in the [Nim](https://nim-lang.org) programming language.

There is one file for each day. The files are named `day01.nim`, `day02.nim` and so on.

For all the programs, the file containing the puzzle input needs to be specified on the command line.

While the event is running, the solution will be committed to this repository after the deadline for that puzzle has expired or a working solution is found, whichever is later.

The code shared here may be slightly different than what was used to actually solve the puzzle, as I may have refactored it or added additional comments.

## Compiling and Running
1. [Install](https://nim-lang.org/install.html) Nim
2. To compile and run a file, proceed as follows (see `nim --fullhelp` for all available options)
	1. For [`day15.nim`](day15.nim), issue a command similar to the one below  
`nim r -d:release day15.nim day15.txt <row> <size>`  
where `day15.txt` is the puzzle input, `<row>` is the row from Part 1 (defaults to 2000000 if omitted) and `<size>` is the maximum value of the grid from Part 2 (defaults to 4000000 if omitted)
	2. For [`day17.nim`](day17.nim), issue a command similar to the one below  
`nim r -d:release day17.nim day17.txt true`  
where `day17.txt` is the puzzle input and the optional `true` argument is used to enable visualizing the output of the calculation
	3. For all other files, issue a command similar to the one below   
`nim r -d:release dayxx.nim dayxx.txt`  
where `dayxx.nim` is the Nim source file and `dayxx.txt` is the puzzle input
