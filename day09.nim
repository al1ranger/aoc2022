# https://adventofcode.com/2022/day/9

import os, strutils, strscans, sets, sequtils

# File must be provided
if os.paramCount() < 1:
  quit(1)

let data = readFile(os.paramStr(1)).strip().splitLines()

type
  Point = tuple[x: int, y: int]

# Gets the number of locations visited by the tail of a rope of knotCount knots 
# at least once
proc getPositionCount(knotCount: int): int =
  var
    knots: seq[Point]
    positions: HashSet[Point] = initHashSet[Point]()
    
  # Every knot is at the origin initially
  knots = repeat((x: 0, y: 0), knotCount)
  positions.incl(knots[knots.high()])
  
  # Process the movement instructions
  for line in data:
    var
      direction: char
      steps: int
    
    # Get direction and number of steps
    if not line.scanf("$c $i", direction, steps):
      continue
    
    for step in 1 .. steps:
      for knot in knots.low() .. knots.high():        
        # Always update position of head
        if knot == knots.low():
          case direction
            of 'U':
              knots[knot].y.inc()
            of 'D':
              knots[knot].y.dec()
            of 'L':
              knots[knot].x.dec()
            of 'R':
              knots[knot].x.inc()
            else:
              discard
          continue
        
        # For all other knots, check if it is adjacent to the previous
        var
          prev: int = knot - 1
        
        if abs(knots[prev].x - knots[knot].x) < 2 and 
          abs(knots[prev].y - knots[knot].y) < 2:
          continue
          
        # If not adjacent, then update position by 1 unit according to the rules,  
        # considering the direction of travel
        if knots[prev].x == knots[knot].x:
          knots[knot].y.inc((knots[prev].y - knots[knot].y) div 
            abs(knots[prev].y - knots[knot].y))
        elif knots[prev].y == knots[knot].y:
          knots[knot].x.inc((knots[prev].x - knots[knot].x) div 
            abs(knots[prev].x - knots[knot].x))
        else:
          knots[knot].y.inc((knots[prev].y - knots[knot].y) div 
            abs(knots[prev].y - knots[knot].y))
          knots[knot].x.inc((knots[prev].x - knots[knot].x) div 
            abs(knots[prev].x - knots[knot].x))
        
        # Store the position of the last knot
        if knot == knots.high():
          positions.incl(knots[knot])
  
  return positions.len()  

# Rope of two knots
block part1:  
  echo "Part 1: ", getPositionCount(2)

# Rope of 10 knots
block part2:
  echo "Part 2: ", getPositionCount(10)
