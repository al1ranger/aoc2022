# https://adventofcode.com/2022/day/13

import os, strutils, json, sequtils, algorithm

# File must be provided
if os.paramCount() < 1:
  quit(1)

type    
  PacketPair = tuple[lhs, rhs: JsonNode]
  
var
  packetPairs: seq[PacketPair] = @[]

# As the input resembles JSON arrays, we will use Nim's JSON parser
block readInput:  
      
  var
    counter: int = 1
  
  packetPairs.add((lhs: nil, rhs: nil))
  
  for line in readFile(os.paramStr(1)).strip().splitLines():      
    if line.len() == 0:
      packetPairs.add((lhs: nil, rhs: nil))
      counter = 1
      continue
    
    if counter == 1:
      packetPairs[packetPairs.high()].lhs = 
        parseJSON("""{"key" : """ & line.strip() & """}""")["key"]
    else:
      packetPairs[packetPairs.high()].rhs = 
        parseJSON("""{"key" : """ & line.strip() & """}""")["key"]
    
    counter.inc()

# The most important thing to realise here is that this is not a "true or false"
# type of comparision but a "less than, equals or greater than" one. 
# The sub-reddit provides more details
# https://www.reddit.com/r/adventofcode/comments/zkmyh4/2022_day_13_solutions/

# Checks if packets represented by JsonNode objects are in correct order, using
# the checks mentioned in the problem. Returns -1, if LHS is lesser, 1 if
# RHS is lesser and 0 if both are equal. For part 2, this will also used as a 
# comparator in the sort function.
proc isOrdered(lhs, rhs: JsonNode): int =
  # Assume equality
  result = 0
  
  # LHS is a single value
  if lhs.kind == JsonNodeKind.JInt:
    # RHS is a single value => Direct comparison
    if rhs.kind == JsonNodeKind.JInt:
      if lhs.getInt() < rhs.getInt():
        return -1
      elif lhs.getInt() > rhs.getInt():
        return 1
      else:
        return 0
    
    # RHS is an array => Create array from LHS and compare
    if rhs.kind == JsonNodeKind.JArray:
      var
        lhsArray: JsonNode = 
          parseJSON("""{"key" : [""" & $lhs.getInt() & """]}""")["key"]
        
      return lhsArray.isOrdered(rhs)
  
  # LHS is an array        
  if lhs.kind == JsonNodeKind.JArray:        
    # RHS is a single value => Create array from RHS and compare
    if rhs.kind == JsonNodeKind.JInt:
      var
        rhsArray: JsonNode = 
          parseJSON("""{"key" : [""" & $rhs.getInt() & """]}""")["key"]
      
      return lhs.isOrdered(rhsArray)
    
    # RHS is an array => Compare items  
    if rhs.kind == JsonNodeKind.JArray:
      # Check if one or both have zero length and calculate result  
      if lhs.len() == 0 and rhs.len() > 0:
        return -1
      elif lhs.len() > 0 and rhs.len() == 0:
        return 1
      elif lhs.len() == 0 and rhs.len() == 0:
        return 0  
      
      # Both arrays have data, so perform item by item comparison
      var
        lhsArray, rhsArray: seq[JsonNode]
        
      lhsArray = lhs.getElems()
      rhsArray = rhs.getElems()  
        
      for idx in lhsArray.low() .. lhsArray.high():
        # Check if RHS is smaller
        if idx > rhsArray.high():
          return 1
        
        # Compare the items
        result = lhsArray[idx].isOrdered(rhsArray[idx])
        if result != 0:
          break
      
      # If values are equal, check whether LHS is longer than RHS
      if result == 0 and lhsArray.high() < rhsArray.high():
        return -1

# Check whether the packet pairs are ordered or not
block part1:          
  # Check whether LHS of a packet pair is lesser than the RHS. Returns -1, if 
  # LHS is lesser, 1 if RHS is lesser and 0 if both are equal.
  proc isOrdered(packetPair: PacketPair): int =
    result = isOrdered(packetPair.lhs, packetPair.rhs)
  
  echo "Part 1: ", countup(packetPairs.low(), packetPairs.high()).toSeq()
    .foldl(if packetPairs[b].isOrdered() == -1: a + b + 1 else: a, 0)

# Add divider packets, sort the list of packets and calculate decoder key
block part2:
  var
    packets: seq[JsonNode] = @[]
  
  for packetPair in packetPairs:
    packets.add(packetPair.lhs)
    packets.add(packetPair.rhs)
  
  packets.add(parseJSON("""{"key" : [[2]]}""")["key"])
  packets.add(parseJSON("""{"key" : [[6]]}""")["key"])
  
  packets.sort(isOrdered)
  
  echo "Part 2: ", countup(packets.low(), packets.high()).toSeq()
    .foldl(if $packets[b] == "[[2]]" or $packets[b] == "[[6]]": a * (b + 1) else: a, 1)
