# https://adventofcode.com/2022/day/7

import os, strutils, strscans, math, algorithm

# File must be provided
if os.paramCount() < 1:
  quit(1)

# Type to store directory data
type
  Node = ref object
    size: int
    name: string
    contents: seq[Node]
    parent: Node

# The root directory
var
  root: Node = nil

block readFile:
  # Function to get a child directory under a parent directory. If a node for
  # that directory does not exist, it will be created
  proc getOrCreateDirectory(parent: Node, directory: string): Node =
    result = nil
    
    # Check if the required node aready exists
    if parent != nil and parent.contents.len() > 0:
      for child in parent.contents:
        if child.name == directory:
          return child
    
    # Node does not exist. So create a new one and return it using the 
    # result variable
    result.new()
    result.parent = parent
    result.name = directory
    result.size = 0
    result.contents = @[]
    
    # Add the current node to the parent's list of children
    if parent != nil:
      parent.contents.add(result)
  
  # Function to add size to the current node and all its parents
  proc addSize(node: Node, size: int) =
    var
      current: Node = node
    
    # Add size to this node and all its parents
    while current != nil:
      current.size.inc(size)
      current = current.parent
  
  # Read the data
  var
    current: Node = nil
  
  for line in readFile(os.paramStr(1)).strip().splitLines():
    case line[0]
      of '$':
        # A command. Either of cd or ls
        var
          command, directory: string

        if line.scanf("$$ $w $*", command, directory):
          case command
            of "cd":
              # Go up one level
              if directory == "..":
                current = current.parent
                continue
              
              # Drop down a level
              current = current.getOrCreateDirectory(directory)
              
              # Set the root
              if directory == "/":
                root = current
            else:
              # Nothing to do for ls
              continue
      of 'd':
        # The node for the directory will be created when processing the 'cd'
        # command. So nothing to be done here.
        continue
      else:
        # This contains the file size. This needs to be added to the current 
        # and parent nodes.
        var
          size: int
          file: string
        
        if line.scanf("$i $*", size, file):
          current.addSize(size)

# For both the parts, we need to collect directory sizes which satisfy a given 
# condition. We will create a function taking a check function as input.
proc collectDirSizes(node: Node, check: proc(node: Node): bool): seq[int]  =
  result = @[]
  
  # Check if the node satisfies the requirement
  if node.check():
    result.add(node.size)
  
  # Carry out the check on the children
  if node.contents.len() > 0:
    for child in node.contents:
      result.add(child.collectDirSizes(check))

# Sum of all directory sizes that are less than or equal to 100000
block part1:    
  echo "Part 1: ", 
    root.collectDirSizes(proc(node: Node): bool = node.size <= 100000).sum()

# The minimum directory size that can be deleted to ensure free space is 
# 30000000 or more (70000000 is the total space)
block part2:
  let
    requiredspace: int = root.size - 40000000 # 30000000 - (70000000 - root.size)
  
  echo "Part 2: ", 
    root.collectDirSizes(proc(node: Node): bool = node.size >= requiredspace)
    .sorted()[0]
