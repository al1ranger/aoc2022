# https://adventofcode.com/2022/day/10

import os, strutils, strscans

# File must be provided
if os.paramCount() < 1:
  quit(1)

var
  commands: seq[tuple[instruction: string, cycles: int, value: int]] = @[]

block readInput:
  for command in readFile(os.paramStr(1)).strip().splitLines():
    var
      instruction, argument: string
      cycles: int
    
    # Extract instruction, the optional value and also set the number of cycles
    if command.scanf("$w$s$*", instruction, argument):
      case instruction
      of "noop":
        cycles = 1
      of "addx":
        cycles = 2
      else:
        continue
      
      # Store instruction details
      if argument.len() > 0:
        commands.add((instruction: instruction, cycles: cycles, 
          value: argument.parseInt()))
      else:
        commands.add((instruction: instruction, cycles: cycles, value: 0))

# Calculate signal strength after 220 cycles
block part1:
  var
    registerX: int = 1
    cycles: int = 0
    signalStrength:int = 0
    
  block exec:
    for command in commands:
      for cycle in 1 .. command.cycles:
        cycles.inc()
        
        # Signal strength is added every odd multiple of 20, i.e. 20 * 1, 
        # 20 * 3 and so on
        if cycles mod 20 == 0 and (cycles div 20) mod 2 == 1:
            signalStrength += (cycles * registerX)
        
        if cycles >= 220:
          break exec
        
      if command.value != 0:
        registerX.inc(command.value)
  
  echo "Part 1: ", signalStrength

# Render the text using the commands
block part2:
  var
    registerX: int = 1
    pixel: int = 0
    line: string
    
  echo "Part 2: "
  
  for command in commands:    
    for cycle in 1 .. command.cycles:                        
      # Determine whether pixel is on or off
      if abs(registerX - pixel) <= 1:
        line &= "#"
      else:
        line &= "."
      
      # Check for end of line
      if pixel == 39:
        # Output current line
        echo line
        
        # Initialize for next line
        pixel = 0
        line = ""
        continue
        
      pixel.inc()
    
    if command.value != 0:
      registerX.inc(command.value)
